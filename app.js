const express = require('express');
const fs = require('fs');
const util = require('util');
const app = express();
const port = 8080;


const logStream = fs.createWriteStream('app_log.txt', { flags: 'a' });
const logStdout = process.stdout;
const logStderr = process.stderr;


console.log = function () {
  logStdout.write(util.format.apply(null, arguments) + '\n');
  logStream.write(util.format.apply(null, arguments) + '\n');
};

console.error = function () {
  logStderr.write(util.format.apply(null, arguments) + '\n');
  logStream.write(util.format.apply(null, arguments) + '\n');
};

app.get('/', (req, res) => {
  res.send('This is my Node application for CICD!!!');
});

app.listen(port, () => {
  console.log(`Application is listening at http://localhost:${port}`);
});